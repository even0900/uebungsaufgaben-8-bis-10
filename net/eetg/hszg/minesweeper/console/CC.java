package net.eetg.hszg.minesweeper.console;

public interface CC {
	String CC_RESET = "\u001B[0m";
	String CC_BLACK = "\u001B[30m";
	String CC_RED = "\u001B[31m";
	String CC_GREEN = "\u001B[32m";
	String CC_YELLOW = "\u001B[33m";
	String CC_BLUE = "\u001B[34m";
	String CC_MAGENTA = "\u001B[35m";
	String CC_CYAN = "\u001B[36m";
	String CC_WHITE = "\u001B[37m";
	String CC_BLACK_BRIGHT = "\u001B[90m";
	String CC_RED_BRIGHT = "\u001B[91m";
	String CC_GREEN_BRIGHT = "\u001B[92m";
	String CC_YELLOW_BRIGHT = "\u001B[93m";
	String CC_BLUE_BRIGHT = "\u001B[94m";
	String CC_MAGENTA_BRIGHT = "\u001B[95m";
	String CC_CYAN_BRIGHT = "\u001B[96m";
	String CC_WHITE_BRIGHT = "\u001B[97m";
	
	String[] MineCC = new String[]{
			CC_RESET,
			CC_CYAN_BRIGHT,
			CC_GREEN,
			CC_RED_BRIGHT,
			CC_BLUE,
			CC_MAGENTA,
			CC_CYAN,
			CC_WHITE,
			CC_WHITE_BRIGHT
	};
}
