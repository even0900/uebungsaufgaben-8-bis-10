package net.eetg.hszg.minesweeper.console;

public class Main {
	
	public static void main(String[] args) {
		
		play();
		
	}
	
	public static void play() {
		
		MineSweeper game = new MineSweeper();
		game.run();
		
	}
}