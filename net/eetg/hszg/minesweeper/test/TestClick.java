package net.eetg.hszg.minesweeper.test;

import net.eetg.hszg.minesweeper.logic.Board;
import net.eetg.hszg.minesweeper.logic.Pos;

public class TestClick {
	
	public static void main(String[] args) {
		
		testLeftClick1();
		testLeftClick2();
		
	}
	
	static void testLeftClick1() {
		
		Board b = new Board(new int[]{4, 6}, 0);
		b.arm(Pos.at(0, 1));
		b.arm(Pos.at(3, 2));
		b.arm(Pos.at(3, 3));
		b.clickLeft(Pos.at(0, 4));
		b.printMatrix();
		
		expectedEqual(b.fieldAt(0, 1).isSwept(), false);
		expectedEqual(b.fieldAt(0, 2).isSwept(), true);
		expectedEqual(b.fieldAt(0, 3).isSwept(), true);
		expectedEqual(b.fieldAt(0, 4).isSwept(), true);
		expectedEqual(b.fieldAt(0, 5).isSwept(), true);
		expectedEqual(b.fieldAt(1, 1).isSwept(), false);
		expectedEqual(b.fieldAt(1, 2).isSwept(), true);
		expectedEqual(b.fieldAt(1, 3).isSwept(), true);
		expectedEqual(b.fieldAt(1, 4).isSwept(), true);
		expectedEqual(b.fieldAt(1, 5).isSwept(), true);
		expectedEqual(b.fieldAt(2, 1).isSwept(), false);
		expectedEqual(b.fieldAt(2, 2).isSwept(), true);
		expectedEqual(b.fieldAt(2, 3).isSwept(), true);
		expectedEqual(b.fieldAt(2, 4).isSwept(), true);
		expectedEqual(b.fieldAt(2, 5).isSwept(), true);
		expectedEqual(b.fieldAt(3, 1).isSwept(), false);
		expectedEqual(b.fieldAt(3, 2).isSwept(), false);
		expectedEqual(b.fieldAt(3, 3).isSwept(), false);
		expectedEqual(b.fieldAt(3, 4).isSwept(), true);
		expectedEqual(b.fieldAt(3, 5).isSwept(), true);
		
	}
	
	static void testLeftClick2() {
		
		Board b = new Board(new int[]{3, 3}, 0);
		b.arm(Pos.at(0, 0));
		b.arm(Pos.at(2, 2));
		b.clickLeft(Pos.at(1, 1));
		b.printMatrix();
		
		expectedEqual(b.fieldAt(0, 0).isSwept(), false);
		expectedEqual(b.fieldAt(0, 1).isSwept(), false);
		expectedEqual(b.fieldAt(0, 2).isSwept(), false);
		expectedEqual(b.fieldAt(1, 0).isSwept(), false);
		expectedEqual(b.fieldAt(1, 1).isSwept(), true);
		expectedEqual(b.fieldAt(1, 2).isSwept(), false);
		expectedEqual(b.fieldAt(2, 0).isSwept(), false);
		expectedEqual(b.fieldAt(2, 1).isSwept(), false);
		expectedEqual(b.fieldAt(2, 2).isSwept(), false);
		
	}
	
	static void expectedEqual(boolean is, boolean should) {
		if (is == should)
			System.out.println("Test okay.");
		else
			System.out.println("Test failed. "
					+ "Expected: " + boolToString(should)
					+ ", fed: " + boolToString(is));
	}
	
	static String boolToString(boolean bool) {
		return bool ? "true" : "false";
	}
	
}
