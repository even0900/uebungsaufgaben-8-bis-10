package net.eetg.hszg.minesweeper.test;

import net.eetg.hszg.minesweeper.logic.Board;
import net.eetg.hszg.minesweeper.logic.Field;
import net.eetg.hszg.minesweeper.logic.Pos;

public class TestBoard {
	
	public static void main(String[] args) {
		
		// errorTest();
		test1();
		test2();
		test3();
		test4();
		
	}
	
	static void errorTest() {
		System.out.println("Test: Fehlerausgabe prüfen");
		Board board = new Board(new int[]{6, 3}, 0);
		board.arm(Pos.at(1, 1));
		board.arm(Pos.at(2, 1));
		
		Pos[] positions = new Pos[]{
				new Pos(0, 1),
				new Pos(1, 2),
				new Pos(1, 1),
		};
		
		expectedMines(board, positions);
		
		MineCheck[] mines = new MineCheck[]{
				MineCheck.at(0, 0, 1),
				MineCheck.at(0, 1, 1),
				MineCheck.at(0, 2, 2),
				MineCheck.at(1, 0, 4),
				MineCheck.at(1, 1, 1),
				MineCheck.at(1, 2, 8),
				MineCheck.at(2, 0, 0),
				MineCheck.at(2, 1, 1),
				MineCheck.at(2, 2, 3),
				MineCheck.at(5, 2, 3)
		};
		
		expectedSurrMines(board, mines);
	}
	
	static void test1() {
		System.out.println("Test: Feld 5x3; 1 Mine auf Pos(1, 1)");
		Board board = new Board(new int[]{5, 3}, 0);
		board.arm(Pos.at(1, 1));
		
		Pos[] positions = new Pos[]{new Pos(1, 1)};
		
		expectedMines(board, positions);
		
		MineCheck[] mines = new MineCheck[]{
				MineCheck.at(0, 0, 1),
				MineCheck.at(0, 1, 1),
				MineCheck.at(0, 2, 1),
				MineCheck.at(1, 0, 1),
				MineCheck.at(1, 1, 1),
				MineCheck.at(1, 2, 1),
				MineCheck.at(2, 0, 1),
				MineCheck.at(2, 1, 1),
				MineCheck.at(2, 2, 1)
		};
		
		expectedSurrMines(board, mines);
	}
	
	static void test2() {
		System.out.println("Test: Feld 3x3; 1 Feld frei, mittig");
		Board board = new Board(new int[]{3, 3}, 0);
		board.arm(Pos.at(0, 0));
		board.arm(Pos.at(0, 1));
		board.arm(Pos.at(0, 2));
		board.arm(Pos.at(1, 0));
		board.arm(Pos.at(1, 2));
		board.arm(Pos.at(2, 0));
		board.arm(Pos.at(2, 1));
		board.arm(Pos.at(2, 2));
		
		Pos[] positions = new Pos[]{
				new Pos(0, 0),
				new Pos(0, 1),
				new Pos(0, 2),
				new Pos(1, 0),
				new Pos(1, 2),
				new Pos(2, 0),
				new Pos(2, 1),
				new Pos(2, 2)
		};
		
		expectedMines(board, positions);
		
		MineCheck[] mines = new MineCheck[]{
				MineCheck.at(1, 0, 5),
				MineCheck.at(1, 1, 8),
				MineCheck.at(2, 2, 3)
		};
		
		expectedSurrMines(board, mines);
	}
	
	static void test3() {
		System.out.println("Test: Feld 3x3; 1 Mine auf Pos(0, 0); 1 Mine auf Pos(2, 2)");
		Board board = new Board(new int[]{3, 3}, 0);
		board.arm(Pos.at(0, 0));
		board.arm(Pos.at(2, 2));
		
		Pos[] positions = new Pos[]{
				Pos.at(0, 0),
				Pos.at(2, 2)
		};
		
		expectedMines(board, positions);
		
		MineCheck[] mines = new MineCheck[]{
				MineCheck.at(0, 0, 1),
				MineCheck.at(0, 1, 1),
				MineCheck.at(0, 2, 0),
				MineCheck.at(1, 0, 1),
				MineCheck.at(1, 1, 2),
				MineCheck.at(1, 2, 1),
				MineCheck.at(2, 0, 0),
				MineCheck.at(2, 1, 1),
				MineCheck.at(2, 2, 1)
		};
		
		expectedSurrMines(board, mines);
	}
	
	static void test4() {
		Board board;
		for (int i = 0; i < 10000; i++) {
			board = new Board(new int[]{9, 9}, 15);
			expectedMineCount(board, 15);
		}
	}
	
	static void expectedMines(Board b, Pos[] mines) {
		for (Field[] row : b.getFieldMatrix())
			for (Field field : row) {
				boolean error = false;
				for (Pos pos : mines)
					if (pos.equals(field)) {
						if (field.isMine()) {
							error = false;
							break;
						} else {
							error = true;
							break;
						}
					} else if (field.isMine())
							error = true;
				if (error) {
					System.out.println("Fehler: " + (!field.isMine() ? "keine " : "" ) + "Mine an Position " + new Pos(field));
				}
			}
	}
	
	static void expectedSurrMines(Board b, MineCheck[] mines) {
		for (MineCheck mine : mines) {
			int surrMines = b.fieldAt(mine.pos).getSurrMines();
			if (surrMines != mine.count)
				System.out.println("Fehler: " +  mine.count + " surrMines erwartet, " + surrMines + " gefunden");
		}
	}
	
	static void expectedMineCount(Board b, int mineCount) {
		int counted = b.countMines();
		if (counted != mineCount)
			System.out.println("Fehler: " + mineCount + " Minen erwartet, " + counted + " erhalten");
		// else System.out.println("Erwartete Minenanzahl korrekt: " + mineCount);
	}
	
}
